      program main
          implicit none
          integer, parameter :: nx = 101, nt = 100
          integer            :: i, n
          real(8)            :: u(nx), un(nx), x(nx)
          real(8)            :: ip1(nx), im1(nx)
          real(8)            :: dx, dt    
          real(8)            :: phi, dphi
          real(8), parameter :: pi = 3.14
          real(8), parameter :: nu = 0.07d0

          dx = 2.d0 * pi / (nx-1)
          dt = dx*nu

          ! Inital and Boundary Conditions
          x(1) = 0.d0
          do i = 2, nx
              ip1(i) = i + 1
              im1(i) = i - 1
              x(i) = x(i-1) + dx
          end do

          do i = 1, nx
              phi = exp( -x(i)**2 / 4.d0 / nu ) + &
                  & exp(-(x(i)-2.d0*pi)**2/4.d0/nu)

              dphi = -0.5d0/nu*x(i) * exp( -x(i)**2/4/nu ) &
                   & - 0.5d0/nu*( x(i) - 2.d0*pi ) * &
                   & exp( -(x(i) - 2.d0*pi)**2/4.d0/nu )

              u(i) = -2.d0*nu*dphi/phi + 4.d0
          end do

          !Finite Difference;
!          un = 1.d0
          do n = 1, nt
              un = u
              do i = 2, nx-1
                  u(i) = un(i) - un(i) * dt/dx * (un(i) - un(i-1)) + &
                       & nu*dt/dx**2*( un(i+1) - 2.d0*un(i) +un(i-1) )
              end do
              u(1) = un(1) - un(1) * dt/dx * (un(1) - un(nx-1)) + &
                   & nu*dt/dx**2*( un(2) - 2.d0*un(1) +un(nx-1) )

              u(nx) = un(nx) - un(nx) * dt/dx * (un(nx) - un(nx-1)) + &
                   & nu*dt/dx**2*( un(1) - 2.d0*un(nx) +un(nx-1) )
           
          end do

          open(7, file='solution.txt')
          do i = 1, nx
              write(7, '(f12.7,a,f12.7)'), x(i), ',',  u(i)
          end do
          close(7)



      end program main
