import numpy
from matplotlib import pyplot
import time, sys

x,y = numpy.loadtxt('solution.txt', delimiter=',', unpack=True)
pyplot.figure(figsize=(11,7), dpi=100)
pyplot.plot(x, y, marker='o', lw=2)
pyplot.show()
pyplot.xlim([0,2*numpy.pi])
pyplot.ylim([0,10])
