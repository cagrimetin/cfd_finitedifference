      module LinearConvection1D
          implicit none
          private
          public linearconv

          contains

          subroutine linearconv(nx)
              integer, intent(in) :: nx
              integer, parameter  :: nt = 20
              integer            :: i, n
              real(8)            :: u(nx), un(nx), x(nx)
              real(8)            :: dx, dt    
              real(8)            :: sigma = .5d0
              real(8), parameter :: c = 1.d0  ! Assumed wavespeed od c = 1

              dx = 2.d0 / (nx-1)
              dt = sigma * dx / c

              ! Inital and Boundary Conditions
              x(1) = 0.d0
              do i = 2, nx
                  x(i) = x(i-1) + dx
              end do

              do i = 1, nx
                  if (x(i) .lt. 1.05d0 .and. x(i) .gt. 0.45d0) then
                      u(i) = 2.d0
                  else
                      u(i) = 1.d0
                  end if
              end do

              !Finite Difference; forward in time
              un = 1.d0
              do n = 1, nt
                  un = u
                  do i = 2, nx-1
                      ! backward difference in space
                      u(i) = un(i) - c*dt/dx*( un(i) - un(i-1) )
                      ! central difference in space
                      !u(i) = un(i) - c*dt/2.d0/dx*( un(i+1) - un(i-1) )
                      ! forward difference in space
                      !u(i) = un(i) - c*dt/2.d0/dx*( un(i+1) - un(i) )
                  end do
              end do

              open(7, file='solution.txt')
              do i = 1, nx
                  write(7, *), x(i), ',',  u(i)
              end do
              close(7)
          end subroutine

      end module
