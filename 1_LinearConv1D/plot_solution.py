import numpy
from matplotlib import pyplot
import time, sys

x,y = numpy.loadtxt('solution.txt', delimiter=',', unpack=True)
pyplot.plot(x, y, 'b', ls='--', lw=3)
pyplot.ylim(0, 2.5)
pyplot.show()
