      program main
          implicit none
          integer, parameter :: nx = 31, ny = 31, nt = 50
          integer            :: i, j, n
          real(8)            :: u(nx,ny), un(nx,ny), x(nx), y(ny)
          real(8)            :: dx, dy, dt    
          real(8), parameter :: sigma = 0.25d0
          real(8), parameter :: nu = 0.3d0

          dx = 2.d0 / (nx-1)
          dy = 2.d0 / (ny-1)
          dt = sigma*dx**2/nu

          ! 2D Grid generation
          x(1) = 0.d0
          do i = 2, nx
              x(i) = x(i-1) + dx
          end do

          y(1) = 0.d0
          do i = 2, ny
              y(i) = y(i-1) + dy
          end do

          ! Inital and Boundary Conditions
          do j = 1, ny
              do i = 1, nx
                  if (x(i) .lt. 1.05d0 .and. x(i) .gt. 0.45d0 .and. &
                     &y(j) .lt. 1.05d0 .and. y(j) .gt. 0.45d0 ) then
                      u(i,j) = 2.d0
                  else
                      u(i,j) = 1.d0
                  end if
              end do
          end do

          !Finite Difference; forward in time, backward in space
          un = 1.d0
          do n = 1, nt
              un = u
              do j = 2, ny-1
                  do i = 2, nx-1
                      u(i,j) = un(i,j) + nu*dt/dx**2* &
                           & ( un(i+1,j) - 2.d0*un(i,j) +un(i-1,j) ) + &
                           & nu*dt/dy**2 * &
                           & ( un(i,j+1) - 2.d0*un(i,j) + un(i,j-1) )
                   end do
              end do
          end do

          open(7, file='solution.txt')
          do j = 1, ny
              do i = 1, nx
                  write(7, *), x(i), ',', y(j), ',',  u(i,j)
              end do
          end do
          close(7)



      end program main
