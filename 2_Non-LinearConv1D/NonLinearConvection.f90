      program main
          implicit none
          integer, parameter :: nx = 41, nt = 20
          integer            :: i, n
          real(8)            :: u(nx), un(nx), x(nx)
          real(8)            :: dx, dt    

          dx = 2.d0 / (nx-1)
          dt = .025d0

          ! Inital and Boundary Conditions
          x(1) = 0.d0
          do i = 2, nx
              x(i) = x(i-1) + dx
          end do

          do i = 1, nx
              if (x(i) .lt. 1.05d0 .and. x(i) .gt. 0.45d0) then
                  u(i) = 2.d0
              else
                  u(i) = 1.d0
              end if
          end do

          !Finite Difference; forward in time, backward in space
          un = 1.d0
          do n = 1, nt
              un = u
              do i = 2, nx-1
                  u(i) = un(i) - un(i)*dt/dx*( un(i) - un(i-1) )
              end do
          end do

          open(7, file='solution.txt')
          do i = 1, nx
              write(7, '(f12.7,a,f12.7)'), x(i), ',',  u(i)
          end do
          close(7)



      end program main
