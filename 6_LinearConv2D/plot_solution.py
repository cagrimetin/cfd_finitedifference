import numpy
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D

x,y,u = numpy.loadtxt('solution.txt', delimiter=',', unpack=True)
n = int(numpy.sqrt(len(x)))
assert n*n == len(x), '"Expected len(x) to be a perfect square, len(x) = %s" % len(x) '

X = x.reshape(n,n)
Y = y.reshape(n,n)
U = u.reshape(n,n)

fig = pyplot.figure(figsize=(11,7), dpi=100)
ax = fig.gca(projection='3d')
surf = ax.plot_surface(X, Y, U)
ax.set_xlabel(' x axis ')
ax.set_ylabel(' y axis ')
ax.set_zlabel(' z axis ')
pyplot.show()
