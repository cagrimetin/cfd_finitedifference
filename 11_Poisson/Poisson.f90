program main
    implicit none
    integer, parameter :: nx = 50, ny = 50, nt = 100
    real(8), parameter :: xmin = 0.d0, ymin = 0.d0, xmax = 2.d0, ymax = 1.d0
    real(8)            :: dx, dy
    real(8), dimension(nx,ny) :: p, pn, b
    real(8), dimension(nx) :: x
    real(8), dimension(ny) :: y
    integer :: i, j, k

    dx = (xmax - xmin) / (ny-1)
    dy = (ymax - ymin) / (ny-1)

    ! 2D Grid generation
    x(1) = 0.d0
    do i = 2, nx
        x(i) = x(i-1) + dx
    end do

    y(1) = 0.d0
    do i = 2, ny
        y(i) = y(i-1) + dy
    end do


    p = 0.d0

    b = 0.d0
    b(nx/4, ny/4) = 100.d0
    b(3*nx/4, 3*ny/4) = -100.d0


    do k = 1, nt
        pn = p
        do j = 2, ny-1
            do i = 2, nx-1
                p(i,j) = ( (pn(i+1,j) + pn(i-1,j) ) * dy**2 + &
                       &   (pn(i,j+1) + pn(i,j-1) ) * dx**2 - &
                       & b(i,j) * dx**2 * dy**2) / (dx**2 + dy**2) / 2.d0
            enddo
        enddo 

        do i = 1, nx
            p(i, 1) = 0.d0
            p(i, nx) = 0.d0
        enddo

        do j = 1, ny
           p(1, j) = 0.d0
           p(ny, j) = 0.d0
        enddo
    enddo


    open(7, file='solution.txt')
    do j = 1, ny
        do i = 1, nx
            write(7, *), x(i), ',', y(j), ',',  p(i,j)
        end do
    end do
    close(7)



end program
