      program main
          implicit none
          integer, parameter :: nx = 81, ny = 81, nt = 100
          integer            :: i, j, n
          real(8)            :: u(nx, ny), un(nx, ny), x(nx), y(ny)
          real(8)            :: dx, dy, dt    
          real(8), parameter :: sigma = 0.2d0
          real(8), parameter :: c = 1.d0  ! Assumed wavespeed od c = 1

          dx = 2.d0 / (nx-1)
          dy = 2.d0 / (ny-1)
          dt = sigma * dx

          ! 2D Grid Generation
          x(1) = 0.d0
          y(1) = 0.d0
           
          do i = 2, nx
              x(i) = x(i-1) + dx
          end do

          do i = 2, ny
              y(i) = y(i-1) + dy
          end do

          ! Inital and Boundary Conditions
          do j = 1, ny
              do i = 1, nx
                  if ( (x(i) .lt. 1.05d0 .and. x(i) .gt. 0.45d0) .and. &
                 & (y(j) .lt. 1.05d0 .and. y(j) .gt. 0.45d0) )then
                      u(i,j) = 2.d0
                  else
                      u(i,j) = 1.d0
                  end if
              end do
          end do

          !Finite Difference; forward in time, backward in space
          un = 1.d0
          do n = 1, nt+1
              un = u
              do j = 2, ny-1
                  do i = 2, nx-1
                      u(i,j) = un(i,j) - c*dt/dx*( un(i,j) - un(i-1,j) ) &
                             & - c*dt/dy*( un(i,j) - un(i,j-1) ) 
                  end do
              end do
          end do

          open(7, file='solution.txt')
          do j = 1, nx
              do i = 1, nx
                  write(7, *), x(i), ',', y(j), ',',  u(i,j)
              end do
          end do
          close(7)



      end program main
