      program main
          implicit none
          integer, parameter :: nx = 41, ny = 41, nt = 120
          integer            :: i, j, n
          real(8)            :: u(nx, ny), un(nx, ny)
          real(8)            :: v(nx, ny), vn(nx, ny)
          real(8)            :: x(nx), y(ny)
          real(8)            :: dx, dy, dt    
          real(8)            :: nu = 0.01d0
          real(8), parameter :: sigma = 0.0009d0

          dx = 2.d0 / (nx-1)
          dy = 2.d0 / (ny-1)
          dt = sigma * dx * dy / nu

          ! 2D Grid Generation
          x(1) = 0.d0
          y(1) = 0.d0
           
          do i = 2, nx
              x(i) = x(i-1) + dx
          end do

          do i = 2, ny
              y(i) = y(i-1) + dy
          end do

          ! Inital and Boundary Conditions
          do j = 1, ny
              do i = 1, nx
                  if ( (x(i) .lt. 1.05d0 .and. x(i) .gt. 0.45d0) .and. &
                 & (y(j) .lt. 1.05d0 .and. y(j) .gt. 0.45d0) )then
                      u(i,j) = 2.d0
                      v(i,j) = 2.d0
                  else
                      u(i,j) = 1.d0
                      v(i,j) = 1.d0
                  end if
              end do
          end do

          !Finite Difference;
          un = 1.d0
          do n = 1, nt
              un = u
              vn = v
              do j = 2, ny-1
                  do i = 2, nx-1
                      u(i,j) = un(i,j) - un(i,j)*dt/dx*( un(i,j) - un(i-1,j) ) &
                             & - vn(i,j)*dt/dy*( un(i,j) - un(i,j-1) ) &
                             & + nu*dt/dx**2* &
                             & (un(i+1,j) - 2.d0*un(i,j) + un(i-1,j)) &
                             & + nu*dt/dy**2* &
                             & (un(i,j+1) - 2.d0*un(i,j) + un(i,j-1))

                      v(i,j) = vn(i,j) - un(i,j)*dt/dx*( vn(i,j) - vn(i-1,j) ) &
                             & - vn(i,j)*dt/dy*( vn(i,j) - vn(i,j-1) ) &
                             & + nu*dt/dx**2* &
                             & (vn(i+1,j) - 2.d0*vn(i,j) + vn(i-1,j)) &
                             & + nu*dt/dy**2* &
                             & (vn(i,j+1) - 2.d0*vn(i,j) + vn(i,j-1))
                  end do
              end do
          end do

          open(7, file='u_solution.txt')
          do j = 1, nx
              do i = 1, nx
                  write(7, *), x(i), ',', y(j), ',',  u(i,j)
              end do
          end do
          close(7)


          open(7, file='v_solution.txt')
          do j = 1, nx
              do i = 1, nx
                  write(7, *), x(i), ',', y(j), ',',  v(i,j)
              end do
          end do
          close(7)

      end program main
