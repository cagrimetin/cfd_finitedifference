import numpy
from matplotlib import pyplot
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

filename = ['solution.txt']
for fname in filename:
    x,y,u = numpy.loadtxt(fname, delimiter=',', unpack=True)
    n = int(numpy.sqrt(len(x)))
    assert n*n == len(x), '"Expected len(x) to be a perfect square, len(x) = %s" % len(x) '

    X = x.reshape(n,n)
    Y = y.reshape(n,n)
    U = u.reshape(n,n)

    fig = pyplot.figure(figsize=(11,7), dpi=100)
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(X, Y, U, rstride=1, cstride=1, cmap=cm.coolwarm, \
            linewidth=0, antialiased=False)

    ax.set_xlim(0, 2)
    ax.set_ylim(0, 1)
#    ax.view_init(30, 225)

    ax.set_title(fname)
    ax.set_xlabel(' x axis ')
    ax.set_ylabel(' y axis ')
    ax.set_zlabel(' z axis ')
    pyplot.show()
