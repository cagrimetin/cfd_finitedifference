import numpy
from matplotlib import pyplot
import time, sys

x,y = numpy.loadtxt('solution.txt', delimiter=',', unpack=True)
pyplot.plot(x, y)
pyplot.show()
